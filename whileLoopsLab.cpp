#include <iostream>
#include <string>
using namespace std;

void Program1()
{
	int maxNumber = 20;
	int initialNumber = 0;
	while (initialNumber < maxNumber + 1)
	{
		cout << initialNumber << " ";
		initialNumber++;
	}
// THIS COMMENT IS STRICTLY FOR AN ASSIGNMENT DO NOT PAY ATTENTION TO THIS PLEASE UNLESS IT IS FOR MY ASSIGNMENT
// COMMENT HAS NOTHING TO DO WITH MY CODE 
}

void Program2()
{
	int maxNumber = 128;
	int initialNumber = 1;
	while (initialNumber < maxNumber + 1)
	{
		cout << initialNumber << " ";
		initialNumber = initialNumber * 2;
	}
}

void Program3()
{
	int favoriteNumber = 6;
	int playerGuess = 0;
	do
	{
		cout<< "Guess a number: ";
		cin >> playerGuess;
		if (playerGuess > favoriteNumber)
		{
			cout << "Too high!\n\n";
		}
		if (playerGuess < favoriteNumber)
		{
			cout << "Too low!\n\n";
		}
		if (playerGuess == favoriteNumber)
		{
			cout << "\nThat's right!\n\n";
		}
	}while(playerGuess != favoriteNumber);

	cout << "GAME OVER";
}

void Program4()
{
	cout << "Please enter a number between 1 and 5: ";
	int userInput;
	cin >> userInput;

	while( userInput < 1 || userInput > 5)
	{
		cout << "Invalid entry, try again: ";
		cin >> userInput;
	}
	cout << "Thanks you.";
}

void Program5()
{
	cout << "What is your starting wage?           ";
	int initialWage;
	cin >> initialWage;

	cout << "What % raise do you get per year?     ";
	int percentRaise;
	cin >> percentRaise;

	cout << "How many years have you worked there? ";
	int yearsWorked;
	cin >> yearsWorked;

	int countYear = 0;
	int adjustedWage = initialWage;


	while( countYear < yearsWorked)
	{
		cout <<"Salary at year " << countYear << ":   " << adjustedWage << endl;
		adjustedWage = ((adjustedWage * percentRaise / 100) + adjustedWage);
		countYear++;
	}
}

void Program6()
{
	cout << "Enter a value for n: ";
	int n;
	cin >> n;
	int counter = 1;
	int sum = 0;

	while (counter <= n)
	{
		sum = sum + counter;
		cout << "Sum: " << sum << endl;
		counter++;
	}
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
